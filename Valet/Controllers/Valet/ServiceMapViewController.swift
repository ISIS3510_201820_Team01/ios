//
//  ServiceMapViewController.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 10/7/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase

class ServiceMapViewController: UIViewController, CLLocationManagerDelegate {
    
    private(set) var servicio:ServicePickUp? = nil
    
    @IBOutlet weak var map: MKMapView!
    
    @IBOutlet weak var fare: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var name: UILabel!
    
    
    var db: Firestore!
    var serviceListener: ListenerRegistration!
    private(set) var ref:CollectionReference? = nil
    
    
    var locationManager = CLLocationManager()
    
    
    
    @IBAction func takeService(_ sender: Any) {
        
        
        
        let  navController = self.tabBarController?.viewControllers![2] as! UINavigationController
        ///secondviewcontroller in your case is cart
        let valetCurrent = navController.viewControllers[0] as! ValetCurrentViewController
        self.tabBarController?.selectedIndex = 2
        // [END setup]
        db = Firestore.firestore()
        
        let da = Date().millisecondsSince1970
        
        let stringQR = (servicio?.client?.id ?? "") + (Auth.auth().currentUser!.uid)
        
        print(servicio?.client?.name ?? "")
        print(servicio?.valet?.id ?? "")
        
        servicio?.serQR(q: stringQR)
        
        db.collection("PickUpServices").document("\(servicio?.serviceId ?? "")").updateData([
            "latitudeValet": servicio?.latitudeValet ?? 0,
            "longitudeValet": servicio?.longitudeValet ?? 0,
            "qr": servicio?.qr,
            "confirmationTime": da,
            "confirmed": true,
            "idValet": Auth.auth().currentUser!.uid
            
            ])
        
        if let ser = servicio
        {
            valetCurrent.updateService(s: ser)
        }
        else{
            print("servicio es nil")
        }
        
        _ = navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        map.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
        else
        {
            print("Please turn on location services or GPS")
        }
        
        // Do any additional setup after loading the view.
        
        let coordinate = CLLocationCoordinate2D(latitude: servicio?.latitudeClient ?? 0.0, longitude: servicio?.longitudeClient ?? 0.0)
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        
        map.addAnnotation(annotation)
        fare.text = "$ " + "\(servicio?.estimatedFare ?? 1500)"
        distance.text = "\(servicio?.getDistance() ?? 0)" + " m"
        name.text = servicio?.client?.name
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: servicio?.latitudeClient ?? 0.0, longitude: servicio?.longitudeClient ?? 0.0), span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        self.map.setRegion(region, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func updateService(s:ServicePickUp){
        servicio = s
    }
    
    // Para que no se voltee la pantalla
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
    }
    
}

extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
        //RESOLVED CRASH HERE
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

