//
//  PickupFollowViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 11/5/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import CoreLocation

class PickupFollowViewController: UIViewController, CLLocationManagerDelegate {

    var ql:ListenerRegistration? = nil
    var db: Firestore! = nil
    var serviceListener: ListenerRegistration! = nil
    private(set) var ref:CollectionReference? = nil
    var servicio:ServicePickUp? = nil
    var locationManager = CLLocationManager()
    var vLatitude = 0.0
    var vLongitude = 0.0
    var timer:Timer? = nil
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var btnConfirmParking: UIButton!
    
    @IBAction func confirmParking(_ sender: Any) {
        
        if let s = servicio {
            
            s.ref?.updateData([
                "parkLatitude": vLatitude,
                "parkLongitude": vLongitude,
                "carParked": true
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
        }
        
        
        // al final manda a la lista
        self.tabBarController?.selectedIndex = 0
        
        //hace pop de todas las vistas
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        self.map.setRegion(region, animated: true)
        
        vLatitude = locations[0].coordinate.latitude
        vLongitude = locations[0].coordinate.longitude
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(PickupFollowViewController.check), userInfo: nil, repeats: true)
        
        map.showsUserLocation = true
        
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }else
        {
            print("Please turn on location services or GPS")
        }
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            let tabBarItem2 = tabArray[2]
            
            tabBarItem1.isEnabled = false
            tabBarItem0.isEnabled = false
            tabBarItem2.isEnabled = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
        timer?.invalidate()
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            
            tabBarItem1.isEnabled = true
            tabBarItem0.isEnabled = true
            
        }
    }
    
    @objc func check()
    {
        if let s = servicio {
            
            s.ref?.updateData([
                "latitudeValet": vLatitude,
                "longitudeValet": vLongitude
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
        }
    }
    
    func updateService(ser:ServicePickUp){
        servicio = ser
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
