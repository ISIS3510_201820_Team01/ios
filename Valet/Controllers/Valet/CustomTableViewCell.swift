//
//  CustomTableViewCell.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/6/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var fecha: UILabel!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var fare: UILabel!
    
    private(set) var servicio:ServicePickUp? = nil
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func updateService(ser:ServicePickUp){
        servicio = ser
    }
    
    
}
