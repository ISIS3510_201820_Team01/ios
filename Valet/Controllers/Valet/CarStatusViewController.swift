//
//  CarStatusViewController.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 11/4/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase

class CarStatusViewController: UIViewController {
    
    private(set) var frontDescription = "" ;
    private(set) var backDescription = "" ;
    private(set) var leftDescription = "";
    private(set) var rightDescription = "";
    private(set) var approved = false;
    
    var servicio:ServicePickUp? = nil;
    var serviceListener:ListenerRegistration!
    
    @IBOutlet weak var left: UIButton!
    @IBOutlet weak var front: UIButton!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var right: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func front(_ sender: Any) {
        let alert = UIAlertController(title: "Parte delantera", message: "Escriba los comentarios que sobre el estado del carro", preferredStyle: UIAlertController.Style.alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { [weak alert] (_) in
            let comment = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            self.frontDescription = comment?.text ?? ""
            
            if(comment?.text != ""){
                self.front.setImage(UIImage(named: "frente check"), for: .normal)
            }
            
            }
        )
        alert.addAction(aceptar)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "..."
            textField.isSecureTextEntry = false
            
            textField.maxLength = 100
            
            
            
        })
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func back(_ sender: Any) {
        let alert = UIAlertController(title: "Parte trasera", message: "Escriba los comentarios que sobre el estado del carro", preferredStyle: UIAlertController.Style.alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { [weak alert] (_) in
            let comment = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            self.backDescription = comment?.text ?? ""
            
            if(comment?.text != ""){
                self.back.setImage(UIImage(named: "atras check"), for: .normal)
            }
            
            
            }
        )
        alert.addAction(aceptar)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "..."
            textField.isSecureTextEntry = false
            
            textField.maxLength = 100
            
            
            
        })
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func leftSide(_ sender: Any) {
        
        let alert = UIAlertController(title: "Parte izquierda", message: "Escriba los comentarios que sobre el estado del carro", preferredStyle: UIAlertController.Style.alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { [weak alert] (_) in
            let comment = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            self.leftDescription = comment?.text ?? ""
            if(comment?.text != ""){
                self.left.setImage(UIImage(named: "izquierdo check"), for: .normal)
            }
                
            
            
            }
        )
        alert.addAction(aceptar)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "..."
            textField.isSecureTextEntry = false
            
            textField.maxLength = 100
            
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func rightSide(_ sender: Any) {
        
        let alert = UIAlertController(title: "Parte derecha", message: "Escriba los comentarios que sobre el estado del carro", preferredStyle: UIAlertController.Style.alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { [weak alert] (_) in
            let comment = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            self.rightDescription = comment?.text ?? ""
            if(comment?.text != ""){
                self.right.setImage(UIImage(named: "derecho check"), for: .normal)
            }
            
            
            }
        )
        alert.addAction(aceptar)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "..."
            textField.isSecureTextEntry = false
            
            textField.maxLength = 100
        })
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func sendComments(_ sender: Any) {
        if (rightDescription == "" || leftDescription == "" || frontDescription == "" || backDescription == "") {
            // Que le avise al man que le falta algún campo
            let alert = UIAlertController(title: "Campos vacios", message: "No has llenado todos los campos", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else {
            //Enviar los datos a la base de datos para que el cliente los confirme
            
            //Cuando el man confirme que pueda continuar con el servicio (osea un listener esperando a que el man confirme jeje)
            if let s = servicio {
                
                s.ref?.updateData([
                    "comments": [self.frontDescription, self.rightDescription, self.backDescription, self.leftDescription]
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Document successfully updated")
                    }
                }
                
                serviceListener = s.ref?.addSnapshotListener(includeMetadataChanges: true) { documentSnapshot, error in
                    guard let docSnapshot = documentSnapshot, docSnapshot.exists else {return}
                    var data = docSnapshot.data()
                    self.approved = data?["approved"] as? Bool ?? false
                    
                    if (self.approved == true){
                        // pasar a la siguiente vista
                        print("Client has approved your comments")
                        
                        let alert = UIAlertController(title: "Éxito", message: "El cliente ha aceptado sus comentarios", preferredStyle: UIAlertController.Style.alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler:{(action) in alert.dismiss(animated: true, completion: nil)
                            
                            self.performSegue(withIdentifier: "park", sender: self)
                            
                        }))
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                let alert = UIAlertController(title: "Éxito", message: "Los comentarios del vehículo fueron guardados satisfactoriamente.", preferredStyle: UIAlertController.Style.alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            let tabBarItem2 = tabArray[2]
            
            tabBarItem1.isEnabled = false
            tabBarItem0.isEnabled = false
            tabBarItem2.isEnabled = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
        if let q = serviceListener {
            q.remove()
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let serviceDetail = segue.destination as! PickupFollowViewController
        
        if let s = servicio{
            serviceDetail.updateService(ser: s)
        }
        
    }

    
    func updateService(ser:ServicePickUp){
        servicio = ser
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
