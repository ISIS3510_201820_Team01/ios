//
//  DropOffListViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/7/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase
import MapKit
import CoreLocation

class DropOffListViewController: UIViewController, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var ql:ListenerRegistration? = nil
    var db: Firestore! = nil
    var serviceListener: ListenerRegistration! = nil
    private(set) var ref:CollectionReference? = nil
    var servicios = [ServiceDropOff]()
    var locationManager = CLLocationManager()
    var vLatitude = 0.0
    var vLongitude = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 70.0
        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }else
        {
            print("Please turn on location services or GPS")
        }
        
        check(com: completition)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func completition(param: [ServiceDropOff]) {
        print(vLatitude)
        servicios = param
        servicios.sort(by: {$0.getDistance() < $1.getDistance()})
        for s in servicios
        {
            s.setValetLocation(latitude: vLatitude, longitude: vLongitude)
        }
        self.tableView.reloadData()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        vLatitude = locations[0].coordinate.latitude
        vLongitude = locations[0].coordinate.longitude
        check(com: completition)
        locationManager.stopUpdatingLocation()
    }
    
    func check(com: @escaping ([ServiceDropOff]) -> Void)
    {
    
        // [END setup]
        db = Firestore.firestore()
       
        
        
        ql = db.collection("DropOffServices").whereField("confirmed", isEqualTo: false)
            .addSnapshotListener { querySnapshot, error in
                guard let documents = querySnapshot?.documents else {
                    print("Error fetching documents: \(error!)")
                    return
                }
                
                var servs = [ServiceDropOff]()
                
                for servicio in documents {
                    
                    
                    let idClient = servicio.data()["idClient"] as! String
                    var client1:Client? = nil
                    
                    let s = ServiceDropOff(pId: servicio.documentID, pCreationTime: NSDate(timeIntervalSince1970: TimeInterval((servicio.data()["creationTime"] as! Double))/1000) as Date,
                                           pEstimatedFare: servicio.data()["estimatedFare"] as! Double ,
                                           pPlate: servicio.data()["plate"] as! String,
                                           pConfirmationTime: NSDate(timeIntervalSince1970: TimeInterval((servicio.data()["confirmationTime"] as! Double))/1000) as Date,
                                           pConfirmed: servicio.data()["confirmed"] as! Bool,
                                           pLatitudeClient: servicio.data()["latitudeClient"] as! Double,
                                           pLongitudeClient: servicio.data()["longitudeClient"] as! Double,
                                           pLatitudeValet: servicio.data()["latitudeValet"] as! Double,
                                           pLongitudeValet: servicio.data()["longitudeValet"] as! Double)
                    
                    servs.append(s)
                    
                    
                    if (idClient != "") {
                        let docRef = self.db.collection("Users").document(idClient)
                        
                        docRef.getDocument { (document, error) in
                            if let document = document, document.exists {
                                client1 = Client(pId: idClient, pName: document.data()?["name"] as! String, pLastName: document.data()?["lastName"] as! String, pEmail: document.data()?["email"] as! String, pPhone: document.data()?["phone"] as! String)
                                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                                print("Document data: \(dataDescription)")
                                
                                
                                s.setClient(c: client1)
                                
                             
                            
                            } else {
                                print("Document does not exist")
                                print("error")
                            }
                            com(servs)
                        }
                    }
                    
                    print(servicio.data()["latitudeValet"] as! Double)
                    //var s:ServiceDropOff? = nil
 
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseI") as! CustomDTableViewCell //1.
        cell.updateService(ser: servicios[indexPath.row])
        
        
        
        
        let fecha = servicios[indexPath.row].creationTime //2.
        let distance = servicios[indexPath.row].getDistance() //2.
        //holiii
        var name = ""//2.
        if let s = servicios[indexPath.row].client {
            name = s.name ?? ""
        }
        let fare = servicios[indexPath.row].estimatedFare //2.
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        dateFormatter.locale = Locale(identifier: "en_US")
        let printDate = dateFormatter.string(from: fecha ?? Date())
        
        cell.fare.text = "$ " + "\(fare ?? 0)"
        cell.fecha.text = printDate
        cell.distance.text = "\(distance)" + " m"
        cell.clientName.text = name
        
        
        return cell //4.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let path = self.tableView.indexPathForSelectedRow
        let row = path?[1]
        
        let serviceDetail = segue.destination as! ServiceMapDropViewController
        let ser = servicios[row!]
        serviceDetail.updateService(s: ser)
    }

    
    // Para que no se voltee la pantalla
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem2 = tabArray[2]
            
            tabBarItem2.isEnabled = false
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        if let q = ql {
            q.remove()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
