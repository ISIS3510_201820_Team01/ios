//
//  ValetHistoryTableViewCell.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/27/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit

class ValetHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var fare: UILabel!
    
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var fecha: UILabel!
    private(set) var servicio:ServicePickUp? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateService(ser:ServicePickUp){
        servicio = ser
    }

}
