//
//  QRGeneratorViewController.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 10/7/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase

class QRGeneratorViewController: UIViewController {
    
    var service:ServicePickUp? = nil
    
    @IBOutlet weak var qr: UIImageView!
    
    var db: Firestore!
    var serviceListener: ListenerRegistration!
    private(set) var ref:DocumentReference? = nil
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("lo que esta en el qr ----------------" + (service?.qr ?? "nada") )
        
        let image = generateQRCode(from: service?.qr ?? "" )
        
        qr.image = image
        
        checkValidation(updateView: updateViewFromModel)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            let tabBarItem2 = tabArray[2]
            
            tabBarItem1.isEnabled = false
            tabBarItem0.isEnabled = false
            tabBarItem2.isEnabled = true
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
        if let q = serviceListener {
            q.remove()
        }
    }
    
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    
    func checkValidation(updateView:@escaping ()-> Void)
    {
        if let s = service{
            if let r = s.ref {
                serviceListener = r.addSnapshotListener { (docSnapshot, error) in
                    guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
                    let data = docSnapshot.data()
                    self.service?.setValidated(pValidated: data?["validated"] as? Bool ?? false)
                    print("HOLA listener validated --> EL NOMBRE QUE BUSCABAS ERA: \(self.service?.validated  ??  false))")
                    updateView()
                }
            }
            else{
                print("Reference is null" )
            }
        }
        else
        {
            print("Servicio es null")
        }
    }
    
    func updateViewFromModel() {
        
        
        if let s = service {
            
            if(s.validated == true){
                // create the alert
                let alert = UIAlertController(title: "¡Servicio Confirmado!", message: "Se ha verificado la validez de su Cliente.", preferredStyle: UIAlertController.Style.alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "Validar condiciones del carro", style: UIAlertAction.Style.default, handler: {(action) in alert.dismiss(animated: true, completion: nil)
                    
                    self.performSegue(withIdentifier: "carValidation", sender: self)
                    
                    
                }))
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let serviceDetail = segue.destination as! CarStatusViewController
        
        if let s = service{
            serviceDetail.updateService(ser: s)
        }
        
    }
    
    func updateService(ser:ServicePickUp){
        service = ser
    }
    
   

}
