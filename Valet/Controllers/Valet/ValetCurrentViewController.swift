//
//  ValetCurrentViewController.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 10/7/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Firebase

class ValetCurrentViewController: UIViewController, CLLocationManagerDelegate {

  
    @IBOutlet weak var map: MKMapView!
    private(set) var servicio:ServicePickUp? = nil
    private(set) var servicioD:ServiceDropOff? = nil
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnFinalizar: UIButton!
    var locationManager = CLLocationManager()
    
    
    @IBAction func FinalizarServicio(_ sender: Any) {
        
        // al final manda a la lista
        self.tabBarController?.selectedIndex = 0
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
          
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
        else
        {
            print("Please turn on location services or GPS")
        }
        
        if (servicio != nil )
        {
            btnFinalizar.isHidden = true
            btnDone.isHidden = false
        }
        else
        {
            btnDone.isHidden = true
            btnFinalizar.isHidden = false
        }
      
        // Add annotation:
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        

        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if (servicio != nil )
        {
        
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: servicio?.latitudeClient ?? 0.0, longitude: servicio?.longitudeClient ?? 0.0), span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            self.map.setRegion(region, animated: true)
            
            if map.annotations.count >= 1 {
                map.removeAnnotations(map.annotations)
            }
            
            let coordinate = CLLocationCoordinate2D(latitude: servicio?.latitudeClient ?? 0.0, longitude: servicio?.longitudeClient ?? 0.0)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            map.addAnnotation(annotation)
        }
        else
        {
            let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: servicioD?.latitudeClient ?? 0.0, longitude: servicioD?.longitudeClient ?? 0.0), span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            self.map.setRegion(region, animated: true)
            
            if map.annotations.count >= 1 {
                map.removeAnnotations(map.annotations)
            }

            let coordinate = CLLocationCoordinate2D(latitude: servicioD?.latitudeClient ?? 0.0, longitude: servicioD?.longitudeClient ?? 0.0)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            map.addAnnotation(annotation)
        }
    }
    
    func updateService(s:ServicePickUp){
        servicioD = nil
        servicio = nil
        servicio = s
       
    }
    
    func updateService(s:ServiceDropOff){
        servicio = nil
        servicioD = nil
        servicioD = s
       
    }
    
    // Para que no se voltee la pantalla
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            let tabBarItem2 = tabArray[2]
            
            tabBarItem1.isEnabled = false
            tabBarItem0.isEnabled = false
            tabBarItem2.isEnabled = true
        }
        
        if (servicio != nil )
        {
            btnFinalizar.isHidden = true
            btnDone.isHidden = false
        }
        else
        {
            btnDone.isHidden = true
            btnFinalizar.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items

        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]

            tabBarItem1.isEnabled = true
            tabBarItem0.isEnabled = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let serviceDetail = segue.destination as! QRGeneratorViewController
        
        if let s = servicio{
           serviceDetail.updateService(ser: s)
        }
        
    }

}
