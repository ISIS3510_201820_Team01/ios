//
//  ServiceDetailViewController.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 10/3/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase

class ServiceDetailViewController: UIViewController {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var stackValues: UIStackView!
    @IBOutlet weak var tEstimado: UILabel!
    @IBOutlet weak var stackCte: UIStackView!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var fareEstimada: UILabel!
    @IBOutlet weak var vPhone: UILabel!
    @IBOutlet weak var valetName: UILabel!
    
    @IBOutlet weak var noInternetMessage: UITextView!
    @IBOutlet weak var btnValidarQR: UIButton!
   
   
    @IBOutlet weak var textWait: UITextView!
    
    
    var service: ServicePickUp? = nil
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var check: UIImageView!
    
    @IBAction func showSecondTab(_ sender: Any) {
        
        let  navController = self.tabBarController?.viewControllers![1] as! UINavigationController
        ///secondviewcontroller in your case is cart
        _ = navController.viewControllers[0] as! MapDropViewController
        //set values you want to pass
        //lets say I want to pass name to secondVC
       
        
        self.tabBarController?.selectedIndex = 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateViewFromModel()
        noInternetMessage.isHidden = true
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            
            tabBarItem1.isEnabled = false
            
        }
        
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            
            noInternetMessage.isHidden = false
            loading.isHidden = true
            textWait.isHidden = true
            btnCancel.isHidden = true
            
            
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func updateService(newService:ServicePickUp?, new:Bool) {
        if new {
            service = newService
            if let s = service {
                s.saveService(updateView: updateViewFromModel)
            }
        }
        else {
            service = newService
            if let s = service {
                s.check(updateView: updateViewFromModel)
            }
        }
    }

    func updateViewFromModel() {
        
        
        
        if let s = service {
            check.isHidden = !s.confirmed
            stackCte.isHidden = !s.confirmed
            stackValues.isHidden = !s.confirmed
            btnValidarQR.isHidden = !s.confirmed
            
            
            if s.confirmed || s.validated {
                loading.stopAnimating()
                textWait.isHidden = true
                btnCancel.isHidden = true
            }
            if s.confirmed && s.validated {
                btnValidarQR.isHidden = true
            }
            tEstimado.text = "\(s.getEstimatedTime())" + " min"
            distance.text = "\(s.getDistance())" + " m"
            fareEstimada.text = "$ " + "\(s.estimatedFare ?? 0)"
            
            
            valetName.text = s.valet?.name
            vPhone.text = s.valet?.phone
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateViewFromModel()
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let s = service {
            s.removeServiceListener()
        }
        else {
            print("Service is nil")
        }
        AppUtility.lockOrientation(.all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "validationArrow" {
            let validate = segue.destination as! ValidateViewController
            validate.updateService(newService: service)
        }
        
        else if segue.identifier == "cancel" {
            let before = segue.destination as! MapViewController
            before.updateService(servicio: nil)
        }
    }
    
    
    @IBAction func cancelService(_ sender: Any) {
        if let s = service
        {
            s.ref?.delete(completion: { (error) in
                if let e = error {
                    print(e)
                }
                else {
                    print("Succesfully deleted")
                }
            })
        }
        
        
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
