//
//  ClientHistoryDetailViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/27/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit

class ClientHistoryDetailViewController: UIViewController {

    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var valetName: UILabel!
    @IBOutlet weak var plate: UILabel!
    @IBOutlet weak var fare: UILabel!
    
    @IBOutlet weak var btnVideo: UIButton!
    
    private(set) var servicio:ServicePickUp? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let fecha = servicio?.creationTime
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateStyle = .none
        dateFormatter2.timeStyle = .medium
        
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter2.locale = Locale(identifier: "en_US")
        
        let printDate = dateFormatter.string(from: fecha ?? Date())
        let printHour = dateFormatter2.string(from: fecha ?? Date())
        
        date.text = printDate
        time.text = printHour
        valetName.text = servicio?.valet?.name
        plate.text = servicio?.plate
        fare.text = "\(servicio?.estimatedFare ?? 1500)"
        
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    
    func updateService(s:ServicePickUp){
        servicio = s
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

