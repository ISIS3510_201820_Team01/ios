//
//  MapViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/2/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController , CLLocationManagerDelegate{
    
    var service:ServicePickUp? = nil
    var client:Client? = nil
    var lat = 0.0
    var lon = 0.0
    var plate = ""
    
    @IBAction func askService(_ sender: Any) {
        
    }
    
    @IBAction func btnCambiarPlaca(_ sender: Any) {
        
        let alert = UIAlertController(title: "¿Qué carro vas a dejar?", message: "Inserta la placa de tu carro.", preferredStyle: UIAlertController.Style.alert)
        let aceptar = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { [weak alert] (_) in
            let pPlate = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            self.plate = pPlate?.text ?? "000000"
            
            }
        )
        alert.addAction(aceptar)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "AAA000"
            textField.isSecureTextEntry = false
            textField.autocapitalizationType = .allCharacters
            textField.maxLength = 7
            
            
            
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var btnAskService: UIButton!
    
    @IBOutlet weak var map: MKMapView!
    
    var locationManager = CLLocationManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            
            tabBarItem1.isEnabled = false
            tabBarItem0.isEnabled = true
            
        }
        //alerta placa
        let alert = UIAlertController(title: "¿Qué carro vas a dejar?", message: "Inserta la placa de tu carro.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { [weak alert] (_) in
            let pPlate = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            self.plate = pPlate?.text ?? "000000"
            
            }
            )
        )
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "AAA000"
            textField.isSecureTextEntry = false
            textField.autocapitalizationType = .allCharacters
            
            textField.maxLength = 6
            
            
        })
        self.present(alert, animated: true, completion: nil)
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        
        map.addGestureRecognizer(tapGesture)
        
        map.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }else
        {
            print("Please turn on location services or GPS")
        }
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    //MAKR:- CLLocationManager Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.map.setRegion(region, animated: true)
        
        lat = locations[0].coordinate.latitude
        lon = locations[0].coordinate.longitude
        
        locationManager.stopUpdatingLocation()
    }
    
    @objc func handleTap(gestureReconizer: UITapGestureRecognizer) {
        
        
        let location = gestureReconizer.location(in: map)
        let coordinate = map.convert(location,toCoordinateFrom: map)
        
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        print(" Coordinates ======= \(coordinate)")
        lat = Double(coordinate.latitude)
        lon = Double(coordinate.longitude)
        
        /* to show only one pin while tapping on map by removing the last.
         If you want to show multiple pins you can remove this piece of code */
        print(map.annotations.count)
        
        if map.annotations.count >= 1 {
            
            map.removeAnnotations(map.annotations)
        }
        
        map.addAnnotation(annotation) // add annotaion pin on the map
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Para que no se voltee la pantalla
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateViewFromModel()
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            let tabBarItem1 = tabArray[1]
            let tabBarItem0 = tabArray[0]
            
            tabBarItem1.isEnabled = false
            tabBarItem0.isEnabled = true
            
        }
        
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let serviceDetail = segue.destination as! ServiceDetailViewController
        if (service == nil)
        {
            service = ServicePickUp(latitude: lat,longitude: lon, pPlate: plate, c: self.client)
        serviceDetail.updateService(newService: service, new: true)
        }
        else
        {
          serviceDetail.updateService(newService: service, new: false)
        }
    }
    
    
    
    func updateViewFromModel() {
        
        
        
        if service == nil {
            btnAskService.setTitle("Pedir servicio", for: .normal)
        }
        else {
         btnAskService.setTitle("Ver servicio", for: .normal)
        }
    }
    
    func updateClient(c: Client?){
        client = c
    }
    
    func updateService(servicio:ServicePickUp?){
        service = servicio
    }

}
