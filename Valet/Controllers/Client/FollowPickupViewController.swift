//
//  FollowPickupViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 11/5/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import CoreLocation

class FollowPickupViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var map: MKMapView!
    
    var ql:ListenerRegistration? = nil
    var db: Firestore! = nil
    var serviceListener: ListenerRegistration! = nil
    private(set) var ref:CollectionReference? = nil
    var servicio:ServicePickUp? = nil
    var locationManager = CLLocationManager()
    var vLatitude = 0.0
    var vLongitude = 0.0
    var timer:Timer? = nil
    
    @IBAction func secondTab(_ sender: Any) {
        
        let  navController = self.tabBarController?.viewControllers![1] as! UINavigationController
        ///secondviewcontroller in your case is cart
        _ = navController.viewControllers[0] as! MapDropViewController
        //set values you want to pass
        //lets say I want to pass name to secondVC
        
        
        self.tabBarController?.selectedIndex = 1
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        map.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() == true {
            
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            
            locationManager.desiredAccuracy = 1.0
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }else
        {
            print("Please turn on location services or GPS")
        }
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        anotationCreation()
        
        if let s = servicio{
            if let r = s.ref {
                serviceListener = r.addSnapshotListener { (docSnapshot, error) in
                    guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
                    let data = docSnapshot.data()
                    
                    
                    self.vLatitude = data?["latitudeValet"] as! Double
                    self.vLongitude = data?["longitudeValet"] as! Double
                    
                    self.anotationCreation()
                    
                }
            }
            else{
                print("Reference is null" )
            }
        }
        else
        {
            print("Servicio es null")
        }
        
        
    }
    
    
    
    func anotationCreation(){
        
        let coordinate = CLLocationCoordinate2DMake(vLatitude, vLongitude)
        
        // Add annotation:
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        
        if map.annotations.count >= 1 {

            map.removeAnnotations(map.annotations)
        }

        map.addAnnotation(annotation)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.009, longitudeDelta: 0.009))
        self.map.setRegion(region, animated: true)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        locationManager.stopUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
  
    
    func updateService(ser:ServicePickUp){
        servicio = ser
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
