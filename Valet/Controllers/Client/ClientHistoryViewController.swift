//
//  ClientHistoryViewController.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 10/27/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase
import UIKit

class ClientHistoryViewController: UIViewController, UITableViewDataSource{
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var ql:ListenerRegistration? = nil
    var db: Firestore! = nil
    var serviceListener: ListenerRegistration! = nil
    private(set) var ref:CollectionReference? = nil
    var servicios = [ServicePickUp]()
    
    
    @IBAction func signOut(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    } 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 70.0
        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        
        check(com: completition)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return servicios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellClientHistory") as! ClientHistoryTableViewCell //1.
        cell.updateService(ser: servicios[indexPath.row])
        
        let fecha = servicios[indexPath.row].creationTime //2.
       
        let name = servicios[indexPath.row].valet?.name
        let fare = servicios[indexPath.row].estimatedFare //2.
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        dateFormatter.locale = Locale(identifier: "en_US")
        let printDate = dateFormatter.string(from: fecha ?? Date())
        
        cell.fare.text = "$ " + "\(fare ?? 0)"
        cell.fecha.text = printDate
        cell.valetName.text = name
        
        
        return cell //4.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
        if let q = ql {
            q.remove()
        }
    }
    
    
    func completition(param: [ServicePickUp]) {
        servicios = param
        servicios.sort(by: {$0.creationTime! > $1.creationTime!})
        self.tableView.reloadData()
    }
    
    func check(com: @escaping ([ServicePickUp]) -> Void)
    {
//        let settings = FirestoreSettings()
////        settings.isPersistenceEnabled = false
////
////        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        ql = db.collection("PickUpServices").whereField("confirmed", isEqualTo: true).whereField("idClient", isEqualTo: Auth.auth().currentUser?.uid)
            .addSnapshotListener { querySnapshot, error in
                guard let documents = querySnapshot?.documents else {
                    print("Error fetching documents: \(error!)")
                    return
                }
                let cities = documents.map { $0["latitudeClient"]! }
                print("Current latitudes: \(cities)")
                
                var servs = [ServicePickUp]()
                
                
                for servicio in documents {
                    
                    let idValet = servicio.data()["idValet"] as! String
                    var valet1:Valet? = nil
                    
                    if (idValet != "") {
                        let docRef = self.db.collection("Users").document(idValet)
                        docRef.getDocument { (document, error) in
                            if let document = document, document.exists {
                                valet1 = Valet(pId: idValet, pName: document.data()?["name"] as! String, pLastName: document.data()?["lastName"] as! String, pEmail: document.data()?["email"] as! String, pPhone: document.data()?["phone"] as! String)
                                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                                print("Document data: \(dataDescription)")
                    
                                let s = ServicePickUp(pId: servicio.documentID,
                                                      pCreationTime: NSDate(timeIntervalSince1970: TimeInterval((servicio.data()["creationTime"] as! Double))/1000) as Date,
                                                      pEstimatedFare: servicio.data()["estimatedFare"] as! Double ,
                                                      pPlate: servicio.data()["plate"] as! String,
                                                      pConfirmationTime: NSDate(timeIntervalSince1970: TimeInterval((servicio.data()["confirmationTime"] as! Double))/1000) as Date,
                                                      pConfirmed: servicio.data()["confirmed"] as! Bool,
                                                      pLatitudeClient: servicio.data()["latitudeClient"] as! Double,
                                                      pLongitudeClient: servicio.data()["longitudeClient"] as! Double,
                                                      pLatitudeValet: servicio.data()["latitudeValet"] as! Double,
                                                      pLongitudeValet: servicio.data()["longitudeValet"] as! Double,
                                                      pQr: servicio.data()["qr"] as? String ?? "")
                                s.setValet(v:valet1)
                                s.setRef(r: servicio.reference)
                                servs.append(s)
                                
                                com(servs)
                            }
                            else {
                                print("Document does not exist")
                                print("error")
                            }
                        }
                    }
                }
              com(servs)
                
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailClient" {
            let path = self.tableView.indexPathForSelectedRow
            let row = path?[1]
            
            let historyDetail = segue.destination as! ClientHistoryDetailViewController
            let ser = servicios[row!]
            historyDetail.updateService(s: ser)
        }
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
