//
//  CarApprovalViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 11/4/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase

class CarApprovalViewController: UIViewController {

    @IBOutlet weak var front: UITextView!
    @IBOutlet weak var back: UITextView!
    @IBOutlet weak var right: UITextView!
    @IBOutlet weak var left: UITextView!
    
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBAction func accept(_ sender: Any) {
        
        if let s = service {
            
            s.ref?.updateData([
                "approved": true
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
        }
        
    }
    
    var service:ServicePickUp? = nil
    var comments = [String]()
    
    var db: Firestore!
    var serviceListener: ListenerRegistration!
    private(set) var ref:DocumentReference? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated:true)
        btnAccept.isEnabled = false
        checkApproval(updateView: updateView)
        
        let alert = UIAlertController(title: "¡Espera un momento!", message: "Tu valet está registrando el estado de tu vehículo", preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    
    func checkApproval(updateView:@escaping ()-> Void)
    {
        
        if let s = service{
            
            if let r = s.ref {
                serviceListener = r.addSnapshotListener { (docSnapshot, error) in
                    guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
                    let data = docSnapshot.data()
                    
                    self.comments = data?["comments"] as! [String]
                    
                    updateView()
                }
            }
            else{
                print("Reference is null" )
            }
        }
        else
        {
            print("Servicio es null")
        }
    }

    func updateView(){
        
        if(comments.count > 0){
            front.text = comments[0]
            right.text = comments[1]
            back.text = comments[2]
            left.text = comments[3]
            
            btnAccept.isEnabled = true
        }
        
    }
    
    func updateService(newService:ServicePickUp?) {
        service = newService
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let serviceDetail = segue.destination as! FollowPickupViewController
        
        if let s = service{
            serviceDetail.updateService(ser: s)
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
