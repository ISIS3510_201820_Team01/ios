//
//  AccountViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/29/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase

class AccountViewController: UIViewController {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password1: UITextField!
    @IBOutlet weak var password2: UITextField!
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var btnSumbit: UIButton!
    
    @IBOutlet var createView: UIView!
    
    var client: Client? = nil
    
    var ref: DocumentReference? = nil
    var db: Firestore! = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db = Firestore.firestore()

        // Do any additional setup after loading the view.
       
        name.attributedPlaceholder = NSAttributedString(string: "Nombre",
                                                               attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        lastName.attributedPlaceholder = NSAttributedString(string: "Apellido",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        email.attributedPlaceholder = NSAttributedString(string: "Correo electrónico",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        phone.attributedPlaceholder = NSAttributedString(string: "Celular",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        password1.attributedPlaceholder = NSAttributedString(string: "Contraseña",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        password2.attributedPlaceholder = NSAttributedString(string: "Repetir contraseña",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        
        name.maxLength = 40
        lastName.maxLength = 40
        email.maxLength = 100
        phone.maxLength = 10
        password1.maxLength = 50
        password2.maxLength = 50
        createView.setGradientBackground(colorOne: Colors.lightBlue, colorTwo: Colors.blue)
        
    createView.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
       
    }
    
    
    @IBAction func createAccount(_ sender: Any) {
        
        //TODO: Falta validar aca que los campos sean correctos
        
        
        
        if (password1.text != password2.text){
            
            print("Aviso al man que las contraseñas son diferentes y paila ")
            let alert = UIAlertController(title: "Error", message: "Las contraseñas no son iguales", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
        }
        
//        if (password1.text?.characters.count ?? 0 > 6){
//
//            let alert = UIAlertController(title: "Error", message: "La contraseña debe tener más de 6 caracteres", preferredStyle: UIAlertController.Style.alert)
//            // add an action (button)
//            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
//            // show the alert
//            self.present(alert, animated: true, completion: nil)
//            return
//        }
        
    
        if (name.text == nil || name.text == "" || name.text == " " || lastName.text == nil || lastName.text == "" || lastName.text == " " || email.text == nil || email.text == "" || email.text == " " || phone.text == nil || phone.text == "" || phone.text == " " || password1.text == nil || password1.text == "" ||  password1.text == " "){
            print("Aviso al man que las contraseñas que algún campo es vacio y paila. ")
            let alert = UIAlertController(title: "Error", message: "Algún campo ingresado está vacio", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        Auth.auth().createUser(withEmail: email.text ?? "", password: password1.text ?? "" ) { (authResult, error) in
            // odio móviles jeje
            if let error = error {
                print(error.localizedDescription)
                let alert = UIAlertController(title: "Error", message:
                    error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
                // show the alert
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            let userID = Auth.auth().currentUser!.uid
            // Add a new document with a generated id.
            
            let docData: [String:Any] = [
                "email": self.email.text ?? "",
                "phone": self.phone.text ?? "",
                "name": self.name.text ?? "",
                "lastName" : self.lastName.text ?? "",
                "client": true
            ]
            
            self.client = Client(pId: userID, pName: self.name.text ?? "", pLastName: self.lastName.text ?? "", pEmail: self.email.text ?? "", pPhone: self.phone.text ?? "")
             self.db.collection("Users").document(String(userID)).setData(docData) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                    let alert = UIAlertController(title: "Error", message: "Error añadiendo el usuario, asegurese que no haya creado una cuenta anteriormente", preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                } else {
                    print("Document added")
                    let alert = UIAlertController(title: "Éxito", message: "Se ha creado la cuenta exitosamente", preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: self.changeView))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }
        
    }
    
    func changeView(action:UIAlertAction){
        self.performSegue(withIdentifier: "client1", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
