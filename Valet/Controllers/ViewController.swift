//
//  ViewController.swift
//  Valet
//
//  Created by Bibiana Gamba on 9/24/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import UIKit
import Firebase



class ViewController: UIViewController {

    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet var signIn: UIView!
    
    var ref: DocumentReference? = nil
    var db: Firestore! = nil
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismiss(animated: false, completion: nil)
        db = Firestore.firestore()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        email.attributedPlaceholder = NSAttributedString(string: "Correo electrónico",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        password.attributedPlaceholder = NSAttributedString(string: "Contraseña",
                                                        attributes: [NSAttributedString.Key.foregroundColor: Colors.placeholderWhite])
        
        email.maxLength = 100
        password.maxLength = 50
    signIn.setGradientBackground(colorOne: Colors.lightBlue, colorTwo: Colors.blue)
        
    signIn.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            // create the alert
            let alert = UIAlertController(title: "¡No tienes Internet!", message: "bip no funciona sin internet, lo sentimos.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
    
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    
    
    @IBAction func signIn(_ sender: Any) {
        
        Auth.auth().signIn(withEmail: email.text ?? "", password: password.text ?? "") { (user, error) in
            // ...
            if let error = error {
                print(error)
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
                // show the alert
                self.present(alert, animated: true, completion: nil)
                return
            }
            print("El usuario ha iniciado sesión con éxito")
            
            //Hacer la consulta si es valet o no para saber a donde lo voy a mandar
            var isClient = false
            
            self.db.collection("Users").whereField("email", isEqualTo: self.email.text ?? "")
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            isClient = document.data()["client"] as? Bool ?? false
                        }
                    }
                    if (isClient)
                    {
                        self.performSegue(withIdentifier: "client1", sender: self)
                    }
                    else
                    {
                        self.performSegue(withIdentifier: "valet1", sender: self)
                    }
            }
            
            
            
        }
    }
    
    
    @IBAction func createAccount(_ sender: Any) {
        
    }
    
}

