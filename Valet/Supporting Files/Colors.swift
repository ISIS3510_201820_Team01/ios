//
//  Colors.swift
//  Valet
//
//  Created by Bibiana Gamba on 10/4/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import Foundation
import UIKit

struct Colors{
    
    //button blue 399DFE
    static let btnBlue = UIColor(red: 57.0/255.0, green: 157.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    
    static let blue = UIColor(red: 0.0/255.0, green: 130.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    
    static let lightBlue = UIColor(red: 64.0/255.0, green: 160.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    
    static let placeholderWhite = UIColor(red: 185.0/255.0, green: 220.0/255.0, blue: 254.0/255.0, alpha: 1.0)
}
