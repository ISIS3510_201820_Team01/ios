//
//  Client.swift
//  Valet
//
//  Created by Nicolás Acevedo Sandoval on 10/3/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import Foundation

class Client {
    
    private(set) var id:String? = nil
    private(set) var name:String? = nil
    private(set) var lastName:String? = nil
    private(set) var email:String? = nil
    private(set) var phone:String? = nil
    
    init(pId: String, pName:String, pLastName:String, pEmail: String, pPhone: String) {
        id = pId
        name = pName
        lastName = pLastName
        email = pEmail
        phone = pPhone
    }
    
}

