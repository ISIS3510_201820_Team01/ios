//
//  Service.swift
//  Valet
//
//  Created by Nicolas Acevedo Sandoval on 10/3/18.
//  Copyright © 2018 Bibiana Gamba. All rights reserved.
//

import Foundation
import Firebase

class ServicePickUp{
    
    var db: Firestore!
    var serviceListener: ListenerRegistration!
    private(set) var ref:DocumentReference? = nil
    
    private(set) var serviceId:String? = nil
    private(set) var client:Client? =  nil
    private(set) var valet:Valet? = nil
    private(set) var creationTime:Date? = nil
    private(set) var estimatedFare:Double? = 1500
    private(set) var plate:String? = nil
    private(set) var confirmationTime:Date? = nil
    private(set) var confirmed = false
    private(set) var latitudeClient:Double? = nil
    private(set) var longitudeClient:Double? = nil
    private(set) var latitudeValet:Double? = nil
    private(set) var longitudeValet:Double? = nil
    private(set) var qr:String? = nil
    private(set) var validated = false
    
    
    private var estimatedTime:Double? {
            if let d = distance {
                return ((d/1.4)/60).rounded()
            }
            else{
                return nil
        }
    }
    
    
    private var distance:Double? {
        if let laC = latitudeClient, let loC = longitudeClient, let laV = latitudeValet, let loV = longitudeValet {
            
            let R = 6371e3
            let radlatValet = Double.pi * (laV/180)
            let radlatClient = Double.pi * (laC/180)
            let theta = ((loC - loV)/180) * Double.pi
            let theta2 = ((laC - laV)/180) * Double.pi
            
            
            let a = sin(theta/2) * sin(theta/2) + cos(radlatClient) * cos(radlatValet) * sin(theta2/2) * sin(theta2/2)
            
            
            var dist = 2 * atan2(sqrt(a), sqrt(1-a))
            
            dist = dist * R
            
            return dist.rounded()
        }
        else
        {
            return nil
        }
    }
    
    func getDistance() -> Double {
        return distance ?? 0
    }
    
    func getEstimatedTime() -> Double {
        return estimatedTime ?? 0
    }
    
    init(pId:String, pCreationTime:Date, pEstimatedFare:Double, pPlate:String, pConfirmationTime:Date, pConfirmed:Bool, pLatitudeClient:Double, pLongitudeClient:Double, pLatitudeValet:Double, pLongitudeValet:Double, pQr:String ) {
        
        serviceId = pId
        creationTime = pCreationTime
        estimatedFare = pEstimatedFare
        plate = pPlate
        confirmationTime = pConfirmationTime
        confirmed = pConfirmed
        latitudeClient = pLatitudeClient
        longitudeClient = pLongitudeClient
        latitudeValet = pLatitudeValet
        longitudeValet = pLongitudeValet
        qr = pQr
        
    }
    
    init(latitude:Double, longitude: Double, pPlate: String, c:Client?)
    {
        latitudeClient = latitude
        longitudeClient = longitude
        plate = pPlate
        client = c
        
        // [END setup]
        db = Firestore.firestore()
    }
    
    func check(updateView:@escaping ()-> Void)
    {
        var idValet = ""
        if let r = ref {
            serviceListener = r.addSnapshotListener { (docSnapshot, error) in
                guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
                let data = docSnapshot.data()
                self.confirmed = data?["confirmed"] as? Bool ?? false
                idValet = data?["idValet"] as? String ?? ""
                print("HOLA --> EL NOMBRE QUE BUSCABAS ERA: \(self.confirmed ))")
                
                var valet1:Valet? = nil
                if (idValet != "") {
                    let docRef = self.db.collection("Users").document(idValet)
                    
                    docRef.getDocument { (document, error) in
                        if let document = document, document.exists {
                            valet1 = Valet(pId: idValet, pName: document.data()?["name"] as! String, pLastName: document.data()?["lastName"] as! String, pEmail: document.data()?["email"] as! String, pPhone: document.data()?["phone"] as! String)
                            let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                            print("Document data: \(dataDescription)")
                        } else {
                            print("Document does not exist")
                            print("error")
                        }
                    }
                    self.valet = valet1
                }
                
                updateView()
            }
        }
        else{
            print("Reference is null" )
        }
        
    }
    
    func saveService(updateView:@escaping ()-> Void){
        var idValet = ""
        ref = nil
        ref = db.collection("PickUpServices").addDocument(data: [
            "confirmed": false,
            "idClient": Auth.auth().currentUser!.uid,
            "idValet": "",
            "plate" : plate ?? "0",
            "latitudeClient" : latitudeClient ?? 0,
            "latitudeValet" : 0,
            "longitudeClient": longitudeClient ?? 0,
            "longitudeValet": 0,
            "creationTime": NSDate().timeIntervalSince1970 * 1000,
            "estimatedFare" : estimatedFare ?? 0,
            "confirmationTime" : 0,
            "qr" : "",
            "validated": false,
            "approved": false,
            "comments": []
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(self.ref!.documentID)")
            }
        }
        
        if let r = ref {
            serviceListener = r.addSnapshotListener { (docSnapshot, error) in
                guard let docSnapshot = docSnapshot, docSnapshot.exists else {return}
                var data = docSnapshot.data()
                self.confirmed = data?["confirmed"] as? Bool ?? false
                self.latitudeValet = data?["latitudeValet"] as? Double ?? 0
                self.longitudeValet = data?["longitudeValet"] as? Double ?? 0
                self.qr = data?["qr"] as? String ?? ""
                idValet = data?["idValet"] as? String ?? ""
                print("HOLA --> EL NOMBRE QUE BUSCABAS ERA: \(self.confirmed ))")
                
                
                var valet1:Valet? = nil
                if (idValet != "") {
                    let docRef = self.db.collection("Users").document(idValet)
                    
                    docRef.getDocument { (document, error) in
                        if let document = document, document.exists {
                            valet1 = Valet(pId: idValet, pName: document.data()?["name"] as! String, pLastName: document.data()?["lastName"] as! String, pEmail: document.data()?["email"] as! String, pPhone: document.data()?["phone"] as! String)
                            let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                            print("Document data: \(dataDescription)")
                            self.valet = valet1
                            updateView()
                        } else {
                            print("Document does not exist")
                            print("error")
                        }
                    }
                    
                }
                
                updateView()
            }
        }
        else{
            print("Reference is null" )
        }
    }
    
    func removeServiceListener()
    {
        serviceListener.remove()
        print("service listener remove")
    }
    
    func setValetLocation(latitude: Double, longitude: Double)
    {
        self.longitudeValet = longitude
        self.latitudeValet = latitude
    }
    
    func setClient(c: Client?){
        self.client = c
    }
    
    func setValet(v: Valet?){
        self.valet = v
    }
    
    func serQR(q: String){
        self.qr = q
    }
    
    func setValidated(pValidated: Bool){
        self.validated = pValidated
    }
    
    func setPlate(pPlate: String){
        self.plate = pPlate
    }
    
    func setRef(r: DocumentReference)
    {
        self.ref = r
    }
}
